# MTTW

MeteoTrentino Wrapper written in Rust.

## Description

This is a simple wrapper for [Meteotrentino](https://www.meteotrentino.it) open data written in Rust. It was meant to be a simple project to learn and get used to Rust.

## Usage

After installation, just run `mttw` in the terminal. You can find more informations about the CLI tool by running:

```shell
mttw --help
```

## Configuration

You can configure your own default location when calling `mttw` by creating a file inside `$XDG_CONFIG_HOME/mttw/config.toml`, which usually expands to `~/.config/mttw/config.toml`. Inside the `config.toml` put:

```toml
[default]
locality = "Trento"
```

When calling `mttw` with no flags it will default the location to your own custom locality.

## Contributing

Although this is a very simple project, feel free to open issues or PRs.
