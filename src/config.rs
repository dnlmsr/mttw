use dirs::config_dir;
use serde_derive::Deserialize;
use toml;

/// Config file main struct
#[derive(Deserialize)]
pub struct Config {
    /// Config file default section
    pub default: Default,
}

/// Config file default section
#[derive(Deserialize)]
pub struct Default {
    /// Default locality
    pub locality: String,
}

impl Config {
    /// Read config from config.toml
    pub fn read_config() -> Option<Self> {
        let config_path = config_dir().unwrap().join("mttw").join("config.toml");

        let config_raw = match std::fs::read_to_string(config_path) {
            Ok(data_raw) => data_raw,
            Err(_) => return None,
        };

        match toml::from_str(&config_raw) {
            Ok(data) => Some(data),
            Err(_) => None,
        }
    }
}
