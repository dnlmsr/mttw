use chrono::prelude::*;
use serde_json::Value;
use std::fmt;
use url::Url;

pub mod config;

#[derive(Debug, PartialEq, Clone)]
pub enum ErrorKind {
    ConnectionError,
    InvalidLocality,
    InvalidUrl,
    ParseError,
}

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
}

impl From<serde_json::Error> for Error {
    fn from(_: serde_json::Error) -> Self {
        Error::new(ErrorKind::ParseError)
    }
}

impl From<reqwest::Error> for Error {
    fn from(_: reqwest::Error) -> Self {
        Error::new(ErrorKind::ConnectionError)
    }
}

impl From<url::ParseError> for Error {
    fn from(_: url::ParseError) -> Self {
        Error::new(ErrorKind::InvalidUrl)
    }
}

impl Error {
    fn new(kind: ErrorKind) -> Self {
        Error { kind }
    }

    pub fn kind(&self) -> ErrorKind {
        self.kind.clone()
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.kind {
            ErrorKind::ConnectionError => write!(f, "Could not connect to Meteotrentino"),
            ErrorKind::InvalidLocality => write!(f, "Invalid locality was provided"),
            ErrorKind::InvalidUrl => write!(f, "Invalid URL"),
            ErrorKind::ParseError => write!(f, "Unable to parse the incoming data"),
        }
    }
}

impl std::error::Error for Error {}

/// The main forecast structure which contains all informations parsed from the official Meteotrentino data
#[derive(Debug)]
pub struct Forecast {
    /// Forecast ID
    pub id: u64,

    /// The specified locality
    pub locality: String,

    /// The height of the locality expressed in meters above sea level
    pub height: u16,

    /// The forecast date and local time
    pub date: DateTime<FixedOffset>,

    /// Vector of available upcoming days
    pub days: Vec<Day>,
}

#[derive(Debug)]
/// This structure contains information regarding the forecast during a specific day
pub struct Day {
    /// The date of the day
    pub date: NaiveDate,

    /// The maximum temperature expressed in °C
    pub temperature_max: i8,

    /// The minimum temperature expressed in °C
    pub temperature_min: i8,

    /// A long description of the weather conditions
    pub description: Option<String>,

    /// Vector of all available time ranges
    pub time_ranges: Vec<TimeRange>,
}

/// This structure contains information regarding the forecast of a given time range.
#[derive(Debug)]
pub struct TimeRange {
    /// The time range expressed in hh-hh
    pub time_range: (DateTime<FixedOffset>, DateTime<FixedOffset>),

    /// A brief description associated to the icon
    pub brief_description: String,

    /// Rain probability expressed with a number between 1 and 4
    pub rain_probability: i8,

    /// Rain intensity expressed with a number between 1 and 4
    pub rain_intensity: i8,

    /// The freezing altitude expressed in meters above sea level
    pub freezing_altitude: u16,

    /// The snow altitude expressed in meters above sea level
    pub snow_altitude: Option<u64>,
}

impl Forecast {
    pub fn get_day(&self, day_to_get: &NaiveDate) -> Option<&Day> {
        self.days.iter().find(|&day| day.date == *day_to_get)
    }
}

fn parse_time_range(
    time_range_raw: &Value,
    day: &NaiveDate,
    timezone: &FixedOffset,
) -> Result<(DateTime<FixedOffset>, DateTime<FixedOffset>), Error> {
    let time_range = time_range_raw
        .as_str()
        .ok_or(Error::new(ErrorKind::ParseError))?;

    let hours_string = time_range.split('-');

    let hours: Vec<NaiveDateTime> = hours_string
        .map(|hour| hour.parse::<u32>().unwrap())
        .map(|hour| match hour {
            24 => day.and_hms_opt(0, 0, 0).unwrap() + chrono::Duration::days(1),
            _ => day.and_hms_opt(hour, 0, 0).unwrap(),
        })
        .collect();

    let start_time: DateTime<FixedOffset> = timezone
        .from_local_datetime(hours.first().unwrap())
        .unwrap();

    let end_time: DateTime<FixedOffset> =
        timezone.from_local_datetime(hours.last().unwrap()).unwrap();

    Ok((start_time, end_time))
}

/// Build TimeRange struct vec from raw data
fn build_time_ranges_from_json(
    time_ranges_raw: &Vec<Value>,
    date: &NaiveDate,
    time_zone: &FixedOffset,
) -> Result<Vec<TimeRange>, Error> {
    let mut time_ranges: Vec<TimeRange> = Vec::new();
    for time_range_raw in time_ranges_raw {
        let time_range = parse_time_range(&time_range_raw["fasciaOre"], date, time_zone)?;

        let rain_probability = time_range_raw["idPrecProb"]
            .as_str()
            .ok_or(Error::new(ErrorKind::ParseError))?
            .parse::<i8>()
            .map_err(|_| Error::new(ErrorKind::ParseError))?;

        let rain_intensity = time_range_raw["idPrecInten"]
            .as_str()
            .ok_or(Error::new(ErrorKind::ParseError))?
            .parse::<i8>()
            .map_err(|_| Error::new(ErrorKind::ParseError))?;

        let freezing_altitude = time_range_raw["zeroTermico"].as_u64().unwrap() as u16;

        let snow_altitude: Option<u64> = match time_range_raw.get("limiteNevicate") {
            Some(value) => Some(value.as_u64().ok_or(Error::new(ErrorKind::ParseError))?),
            None => None,
        };

        let brief_description = time_range_raw["descIcona"]
            .as_str()
            .ok_or(Error::new(ErrorKind::ParseError))?
            .to_string();

        time_ranges.push(TimeRange {
            time_range,
            brief_description,
            rain_intensity,
            rain_probability,
            freezing_altitude,
            snow_altitude,
        });
    }
    Ok(time_ranges)
}

/// Build Day struct vec from raw data
fn build_days_from_json(days_raw: &Vec<Value>, time_zone: &FixedOffset) -> Result<Vec<Day>, Error> {
    let mut days: Vec<Day> = Vec::new();

    for day_raw in days_raw {
        let date = NaiveDate::parse_from_str(
            day_raw["giorno"]
                .as_str()
                .ok_or(Error::new(ErrorKind::ParseError))?,
            "%Y-%m-%d",
        )
        .map_err(|_| Error::new(ErrorKind::ParseError))?;

        let temperature_max = day_raw["tMaxGiorno"]
            .as_i64()
            .ok_or(Error::new(ErrorKind::ParseError))? as i8;

        let temperature_min = day_raw["tMinGiorno"]
            .as_i64()
            .ok_or(Error::new(ErrorKind::ParseError))? as i8;

        let description_raw = day_raw["testoGiorno"]
            .as_str()
            .ok_or(Error::new(ErrorKind::ParseError))?;

        let description: Option<String> = match description_raw {
            "" => None,
            _ => Some(description_raw.to_string()),
        };

        let time_ranges_raw = day_raw["fasce"]
            .as_array()
            .ok_or(Error::new(ErrorKind::ParseError))?;

        let time_ranges = build_time_ranges_from_json(time_ranges_raw, &date, time_zone)?;

        download_icon(Url::parse(day_raw["icona"].as_str().unwrap()).unwrap());

        days.push(Day {
            date,
            description,
            temperature_min,
            temperature_max,
            time_ranges,
        });
    }

    Ok(days)
}

/// Build Forecast struct from raw data
fn build_forecast_from_json(body: &str) -> Result<Forecast, Error> {
    let raw_data: Value = serde_json::from_str(body)?;

    let previsione = raw_data["previsione"][0]
        .as_object()
        .ok_or(Error::new(ErrorKind::InvalidLocality))?;

    let date = DateTime::parse_from_str(
        raw_data["dataPubblicazione"]
            .as_str()
            .ok_or(Error::new(ErrorKind::ParseError))?,
        "%Y-%m-%dT%H:%M%z",
    )
    .map_err(|_| Error::new(ErrorKind::ParseError))?;

    let id = raw_data["idPrevisione"]
        .as_u64()
        .ok_or(Error::new(ErrorKind::ParseError))?;

    let locality = previsione["localita"]
        .as_str()
        .ok_or(Error::new(ErrorKind::ParseError))?
        .to_string();

    let height = previsione["quota"]
        .as_u64()
        .ok_or(Error::new(ErrorKind::ParseError))? as u16;

    let days_raw = previsione["giorni"]
        .as_array()
        .ok_or(Error::new(ErrorKind::ParseError))?;

    let days = build_days_from_json(days_raw, &date.timezone())?;

    Ok(Forecast {
        id,
        date,
        days,
        locality,
        height,
    })
}

/// Fetch weather data from meteotrentino site
pub async fn fetch_weather_data(locality: &str) -> Result<Forecast, Error> {
    let params = [("localita", locality)];

    let url = Url::parse_with_params(
        "https://www.meteotrentino.it/protcivtn-meteo/api/front/previsioneOpenDataLocalita",
        params,
    )?;

    let body = reqwest::get(url).await?.text().await?;

    build_forecast_from_json(&body)
}

/// Download icon and store it in cache
fn download_icon(icon_url: Url) {
    let icon_filename = icon_url.path_segments().unwrap().last().unwrap();

    let icons_directory = dirs::cache_dir().unwrap().join("mttw").join("icons");
    std::fs::create_dir_all(&icons_directory).expect("Unable to create directory");
    let icon_path = icons_directory.join(icon_filename);

    if !std::path::Path::new(&icon_path).exists() {
        let mut file = std::fs::File::create(icon_path).expect("Failed opening file");
        reqwest::blocking::get(icon_url)
            .unwrap()
            .copy_to(&mut file)
            .expect("Failed downloading image");
    }
}
