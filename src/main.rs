use chrono::{Days, Local, Timelike};
use clap::{Parser, Subcommand};
use mttw::config::Config;
use prettytable::{row, Attr, Cell, Row, Table};
use std::vec;

/// Meteotrentino wrapper
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Args {
    /// Name of the locality
    #[clap(global = true, short, long, value_parser)]
    locality: Option<String>,

    /// CLI subcommands
    #[clap(subcommand)]
    command: Option<Commands>,
}

/// CLI subcommands
#[derive(Subcommand, Debug)]
enum Commands {
    /// Show today weather
    Today,

    /// Show tomorrow weather
    Tomorrow,
}

fn get_locality(args: &Args) -> String {
    match &args.locality {
        Some(locality) => locality.clone(),
        None => match Config::read_config() {
            Some(config) => config.default.locality,
            None => "Trento".to_string(),
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), mttw::Error> {
    let args = Args::parse();

    let locality = get_locality(&args);

    let forecast = mttw::fetch_weather_data(&locality).await?;

    println!("Weather forecast for: {}", forecast.locality);

    let time_now = Local::now();
    let time_difference = time_now.time() - forecast.date.time();
    println!(
        "Last forecast update was {} hours and {} minutes ago.",
        time_difference.num_hours(),
        time_difference.num_minutes() % 60
    );

    let today = time_now.date_naive();
    let day = match &args.command {
        None | Some(Commands::Today) => forecast.get_day(&today).unwrap(),
        Some(Commands::Tomorrow) => forecast
            .get_day(&today.checked_add_days(Days::new(1)).unwrap())
            .unwrap(),
    };

    let mut table = Table::new();
    table.add_row(Row::new(vec![Cell::new("Day forecast")
        .with_style(Attr::Bold)
        .with_hspan(5)]));

    table.add_row(row!["Min temperature", "Max temperature"]);
    table.add_row(row![
        format!("{}°C", day.temperature_min),
        format!("{}°C", day.temperature_max),
    ]);

    table.add_row(Row::new(vec![Cell::new("Time forecast")
        .with_style(Attr::Bold)
        .with_hspan(5)]));
    table.add_row(row![
        "Time range",
        "Rain probability",
        "Rain intensity",
        "Freezing altitude",
        "Snow altitude"
    ]);
    for time_range in &day.time_ranges {
        let range = format!(
            "{}-{}",
            time_range.time_range.0.hour(),
            time_range.time_range.1.hour()
        );
        table.add_row(row![
            range,
            format!("{}%", time_range.rain_probability * 25),
            format!("{}%", time_range.rain_intensity * 25),
            format!("{}m", time_range.freezing_altitude),
            match time_range.snow_altitude {
                Some(snow_altitude) => format!("{}m", snow_altitude),
                None => String::from("-"),
            },
        ]);
    }

    table.printstd();

    Ok(())
}
