fn validate_day(day: &mttw::Day) {
    assert!(day.temperature_max > day.temperature_min);
    assert!(day.temperature_min > -100);
    assert!(day.temperature_max < 100);
    match &day.description {
        Some(description) => assert!(description.chars().count() > 10),
        _ => (),
    }
}

fn validate_today(day: &mttw::Day) {
    assert!(day.temperature_max > day.temperature_min);
    assert!(day.temperature_min > -100);
    assert!(day.temperature_max < 100);
    assert!(day.description.as_ref().unwrap().chars().count() > 10);
}

fn validate_forecast(forecast: &mttw::Forecast) {
    assert!(forecast.days.len() > 2);

    for day in &forecast.days {
        validate_day(&day);
    }

    validate_today(&forecast.days[0]);
}

#[tokio::test]
async fn test_correct_forecast_names() {
    let localities = vec![
        "TRENTO",
        "Trento",
        "Trento  ",
        "trento",
        "rifugio pedrotti  alla tosa",
    ];
    for locality in localities {
        assert!(mttw::fetch_weather_data(locality).await.is_ok());
    }
}

#[tokio::test]
async fn test_incorrect_forecast_names() {
    let locality = "undefined";
    assert_eq!(
        mttw::fetch_weather_data(locality).await.unwrap_err().kind(),
        mttw::ErrorKind::InvalidLocality
    );
}

#[tokio::test]
async fn test_forecast_validity() {
    let locality = "Trento";
    let forecast = mttw::fetch_weather_data(locality).await.unwrap();
    validate_forecast(&forecast);
}

#[tokio::test]
async fn test_get_day() {
    let locality = "Trento";
    let forecast = mttw::fetch_weather_data(locality).await.unwrap();

    let now = chrono::Local::now();

    let day = forecast.get_day(&now.date_naive()).unwrap();

    assert_eq!(now.date_naive(), day.date);
    validate_today(day);
}
